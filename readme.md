# EC1 Extra Child - Starter Child Theme

__Contributors:__ 

* Tim Russell [Web](http://timrussell.com) / [GitLab](https://gitlab.com/tdavidrussell)    

__Requires:__ 5.0   
__Tested up to:__ 5.3   
__License:__ [GPL-2.0+](http://www.gnu.org/licenses/gpl-2.0.html)    
__[Project Page](https://gitlab.com/tdavidrussell/ec1)__   


## Summary
EC1 Extra Child (starter) Child Theme.  This theme will work as is, though this is not my intention for this theme. 

Download theme install on local development website, duplicate theme and rename. Modified to your needs. 

Please note the changes made:   
	1. Social icons will open in new browser window  
	
[Extra Parent Theme](https://www.elegantthemes.com/) REQUIRED!

## License   
This program is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE. See the GNU General Public License for more details.


## Installation


### Upload ###

1. Download the [latest release](https://gitlab.com/tdavidrussell/ec1) from GitLab.
2. Go to the __Appearance &rarr; Themes &rarr; Add New__ screen in your WordPress admin panel and click the __Upload__ tab at the top.
3. Upload the zipped archive.
4. Click the __Activate Theme__ link after installation completes.

### Manual ###

1. Download the [latest release](https://gitlab.com/tdavidrussell/ec1) from GitLab.
2. Unzip the archive.
3. Copy the folder to ' /wp-content/themes/ '.
4. Go to the __Appearance &rarr; Themes__ screen in your WordPress admin panel and click the __Activate__ button on EC1 Extra Child (Starter Theme) theme.

Read the Codex for more information about [installing theme manually](https://codex.wordpress.org/Using_Themes).

### Usage ###


## Changelog

See [CHANGELOG](changelog.md).
