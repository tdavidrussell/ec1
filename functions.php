<?php

/**
 * Child Theme Functions
 *
 * Theme extra Functions
 *
 * @category            WordPress_Theme
 * @package             EC1_Extra_Child
 * @subpackage          theme
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Load Extra parent stylesheet....
 */
function rone_enqueue_theme_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'rone_enqueue_theme_styles' );


?>